// SPDX-License-Identifier: LGPL-3.0-or-later

/* Trivial osx_dir.h implementation on Unix.
 *
 * Copyright: 2022, The DoubleFourteen Code Forge
 * Author: Lorenzo Cogotti
 */

#include "xconf.h"
#include "osx_dir.h"

DF_OSXLIB_API df_os_dirhn os_opendir(const char *name)
{
    return opendir(name);
}

DF_OSXLIB_API char *os_readdir(df_os_dirhn hn)
{
    struct dirent *ent = readdir(hn);
    return ent ? ent->d_name : (char *) 0;
}

DF_OSXLIB_API void os_closedir(df_os_dirhn hn)
{
    (void) closedir(hn);
}

