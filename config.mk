# Compiler and tools
CC = cc
LD = $(CC)
LUA = lua
LDOC = ldoc
PKGCONFIG = pkg-config
CP = cp
RM = rm -f
MKDIR = mkdir
RMDIR = rmdir

# Compilation flags
CFLAGS = -pipe -Os -fPIC -Wall -pedantic
# Linking options
LDFLAGS = -W -O1

# Lua setup
LUA_INCDIR = $(shell $(PKGCONFIG) --variable=includedir $(LUA))
LUA_LIBDIR = $(shell $(PKGCONFIG) --variable=libdir $(LUA))
LIBDIR = $(shell $(PKGCONFIG) --variable=INSTALL_CMOD $(LUA))
