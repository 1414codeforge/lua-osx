package = "osx"
version = "scm-1"
source = {
    url = "git+https://gitea.it/1414codeforge/lua-osx"
}
description = {
    summary = "Low level Operating System eXtensions for the Lua Programming Language",
    detailed = [[
        lua-osx is a low level Lua module extending the standard os library to
        provide additional consistent and portable basic system level facilities
        to Lua.
        Such as directory traversal, essential file stat functionality and
        file manipulation.
        Separate headers and API are also provided to interact with system
        directories from C in other modules.
    ]],
    license = "LGPL-3-or-later"
}
dependencies = {
    "lua >= 5.1"
}
build = {
    type = "make",
    build_variables = {
         LUA="$(LUA)",
         CC="$(CC)",
         LD="$(LD)",
         CFLAGS="$(CFLAGS)",
         LIBFLAG="$(LIBFLAG)",
         LUA_INCDIR="$(LUA_INCDIR)",
         LUA_LIBDIR="$(LUA_LIBDIR)"
      },
      install_variables = {
         CP="$(CP)",
         RM="$(RM)",
         MKDIR="$(MKDIR)",
         RMDIR="$(RMDIR)",
         LIBDIR="$(LIBDIR)"
      }
}
