Operating System eXtensions for Lua
===================================

A C module to provide a few fundamental system-specific facilities to the
Lua language.

This library provides means to:
- traverse and filter directory contents;
- retrieve basic file information (type, size, modification time, ...);
- create directories;
- advanced file interaction (change size and mode, lock files, advise kernel on file access pattern, ...).

## Why?

Mainline Lua only exposes functionality when directly
available through the standard C library (with the notable exception of `popen()`).
This is justified by the embeddable nature of Lua.

Yet, a minimum degree of system-specific interaction is needed to complete
basic to advanced general purpose scripting and programmatic tasks.
**lua-osx** attempts to fill this gap.

## Philosophy

The library adheres the following guidelines:
* only provide essential and reasonably portable functionality;
* integrate seamlessly with the official Lua library
  (e.g. support Lua files as arguments, behave consistently with the Lua standard library);
* avoid functionality only available in one system (e.g. providing a Linux-only function).

These are intended to be general principles, exceptions may apply when justified.

## Compatibility

**lua-osx** is portable with most relevant Lua implementations, specifically it is
tested on [PUC Rio Lua](https://www.lua.org) and [LuaJIT](https://luajit.org).
Code is known to work on Linux and Windows, and it should be portable to any POSIX
system (BSD and Mac).

## Build and install

**lua-osx** comes with a `Makefile` and a [LuaRocks](https://luarocks.org) file.

Windows users should note that **lua-osx** does not support MSVC and never will.
It requires a fairly modern C compiler with C11 support and minimal Unix-style
shell tools. On Windows you may use MSYS with either MinGW or clang.

### Using LuaRocks

You can build and install **lua-osx** locally on for your user, by
opening a shell in the project's root directory and issuing the following
command:
```sh
$ luarocks build --local
```

Or alternatively build and install it system-wide with:
```sh
# luarocks build --global
```

### Building using Makefile

**lua-osx** rock uses the project Makefile to do build the module.

The Makefile may also be used stand-alone, and support a number
of configurations to adapt to the different supported systems.
These may be picked with the `CONFIG` variable.

* **Building on GNU/Linux and regular Unix-like systems**

  The regular Unix configuration is used by default, thus
  you may simply run:
  ```sh
  $ make
  ```

  Or use the extended (equivalent) form:
  ```sh
  $ make CONFIG=config.unix
  ```

* **Building on Windows**

  You may build **lua-osx** using any fairly recent C compiler
  with C11 support (e.g. MinGW or clang), and Unix-style shell
  utilities and `make` (e.g. MSYS and GNU make).
  ```sh
  $ make CONFIG=config.win
  ```

* **Building on Mac**

  To build **lua-osx** on Mac select the `config.macosx` configuration:
  ```sh
  $ make CONFIG=config.macosx
  ```

### Installing using Makefiles

**lua-osx** Makefile supports your average `clean`, `doc`, `install` and `uninstall`
targets, thus you can install **lua-osx** system wide by running:
```sh
# make install
```

## Documentation

**lua-osx** uses [LDoc](https://github.com/lunarmodules/LDoc) for documentation.

You can generate documentation using `ldoc` directly, running the following command:
```sh
$ ldoc .
```

Or using the `doc` `Makefile` target:
```sh
$ make doc
```

Both methods generate a `doc` directory, open `doc/index.html`
with your favorite browser to read the API documentation.

## Similar Projects

* [LuaFileSystem](https://lunarmodules.github.io/luafilesystem)

## License

**lua-osx** is free software.
You can redistribute and/or modify it under the terms of the GNU Lesser General Public License.
See [LICENSE file](LICENSE) for details.

