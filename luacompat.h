// SPDX-License-Identifier: LGPL-3.0-or-later

/* Compatibility layer with Lua 5.1/LuaJIT.
 *
 * Copyright: 2022, The DoubleFourteen Code Forge
 * Author: Lorenzo Cogotti
 */

#ifndef DF_LUA_LUACOMPAT_H_
#define DF_LUA_LUACOMPAT_H_

#include "lua.h"
#if LUA_VERSION_NUM < 502
#include <stdio.h>

typedef struct {
    FILE *f;
} luaL_Stream;

static inline void luaL_requiref(lua_State *L, const char *modname, lua_CFunction openf, int glb)
{
    lua_pushstring(L, "_LOADED");
    lua_gettable(L, LUA_REGISTRYINDEX);
    if (lua_isnil(L, -1)) {
        lua_pop(L, 1);
        lua_createtable(L, 0, 1);
        lua_pushstring(L, "_LOADED");
        lua_pushvalue(L, -2);
        lua_settable(L, LUA_REGISTRYINDEX);
    }

    lua_getfield(L, -1, modname);
    if (lua_isnil(L, -1)) {
        lua_pop(L, 1);
        lua_pushcfunction(L, openf);
        lua_pushstring(L, modname);
        lua_call(L, 1, 1);
        lua_pushvalue(L, -1);
        lua_setfield(L, -3, modname);
    }
    if (glb) {
        lua_pushvalue(L, -1);
        lua_setglobal(L, modname);
    }
    lua_replace(L, -2);
}

#endif

#if LUA_VERSION_NUM < 503
#define luaL_pushfail(L) lua_pushnil(L)
#endif

#endif
