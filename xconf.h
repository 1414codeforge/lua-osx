// SPDX-License-Identifier: LGPL-3.0-or-later

/* Cross platform configuration.
 *
 * Deals with platform and compiler specific definitions.
 * Must be included first in every .c source. 
 *
 * Copyright 2022, The DoubleFourteen Code Forge
 * Author: Lorenzo Cogotti
 */

#ifndef DF_LUA_PREFIX_H_
#define DF_LUA_PREFIX_H_

#ifdef __unix__
#define _POSIX_C_SOURCE 200809L
#ifdef _AIX
#define _LARGE_FILES 1
#else
#define _FILE_OFFSET_BITS 64
#endif

#define _LARGEFILE64_SOURCE
#endif

#endif
