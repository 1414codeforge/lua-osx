// SPDX-License-Identifier: LGPL-3.0-or-later

/* Lua OS extensions, low level system facilities.
 *
 * Copyright: 2022, The DoubleFourteen Code Forge
 * Author: Lorenzo Cogotti
 */

#ifndef DF_LUA_OSX_H_
#define DF_LUA_OSX_H_

#include "lua.h"

#ifdef _WIN32

#if defined(DF_LUA_OSXLIB)
#define DF_OSXMOD_API __declspec(dllexport)
#else
#define DF_OSXMOD_API __declspec(dllimport)
#endif

#else
#define DF_OSXMOD_API
#endif

typedef struct {
    void *hn;         // df_os_dirhn - see osx_dir.h
    _Bool closeflag;  // whether the directory has been closed
} df_os_dir;

#define DF_LUA_DIRHANDLE "DIR*"
#define DF_LUA_OSXLIBNAME "osx"

DF_OSXMOD_API int luaopen_osx(lua_State *L);

#endif
