# The lua-osx Makefile
# Author: Lorenzo Cogotti, The DoubleFourteen Code Forge
#
# For general configuration see config.mk.
# For system specific configuration see other config.*s.
#
# To pick a specific system configuration use the CONFIG variable
# (config.unix by default), e.g.:
# make CONFIG=config.macosx
#
# By default we build against system's lua interpreter,
# to build against other implementations use the LUA variable, e.g.:
# make LUA=luajit
#
# To manually specify lua's include path, library path and library name use:
# LUA_INCDIR, LUA_LIBDIR and LUALIB variables respectively.
# By default these are automatically detected via pkg-config.
#
# To manually specify the module install path use the LIBDIR variable,
# By default it is automatically detected via pkg-config.
#
# NOTE: there is NO support for the MSVC compiler.

CONFIG = config.unix

include config.mk
include $(CONFIG)

lib = osx.$(LIB_EXTENSION)
src = osx.c
o = $(src:.c=.o)
_o = $(_src:.c=.o)

.c.o:
	$(CC) -std=c11 $(_cflags) $(CFLAGS) -c $<

all: $(lib)

$(_o): $(_src) xconf.h osx_dir.h

osx.o: osx.c xconf.h luacompat.h osx.h osx_dir.h

$(lib): $(o) $(_o)
	$(LD) $(LIBFLAG) -o $@ $(_ldflags) $(LDFLAGS) $(o) $(_o)

doc/index.html: README.md config.ld osx.c
	$(LDOC) .

doc: doc/index.html

install:
	-$(MKDIR) $(DESTDIR)$(LIBDIR)/include
	$(CP) $(lib) $(DESTDIR)$(LIBDIR)/
	$(CP) osx.h osx_dir.h $(DESTDIR)$(LIBDIR)/include/

uninstall:
	-$(RM) $(DESTDIR)$(LIBDIR)/$(lib)
	-$(RM) $(DESTDIR)$(LIBDIR)/include/osx.h $(DESTDIR)$(LIBDIR)/include/osx_dir.h
	-$(RMDIR) $(DESTDIR)$(LIBDIR)/include

clean:
	-$(RM) -R doc
	-$(RM) $(lib) $(o) $(_o)
